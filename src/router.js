import Vue from 'vue'
import Router from 'vue-router'
import Auth from './helpers/Auth.js'

import Login from './pages/Login.vue'
import Jogo from './pages/Jogo.vue'
import Placares from './pages/Placares.vue'

/*import UsuariosOnline from './pages/UsuariosOnline.vue'
import Aplicacoes from './pages/Aplicacoes.vue'
import Clientes from './pages/Clientes.vue'
import Licencas from './pages/Licencas.vue'
import Planos from './pages/Planos.vue'
import Error404 from './pages/Error404.vue'

import Pagamentos from './pages/Pagamentos.vue'
import Revendas from './pages/Revendas.vue'
import RevendaNormalizador from './components/RevendaNormalizador.vue'
import RevendaAstandes from './components/RevendaAstandes.vue'*/

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'login',
      component: Login
    },
    {
      path: '/jogo',
      name: 'jogo',
      component: Jogo
    },
    {
      path: '/placares',
      name: 'placares',
      component: Placares
    },
    /*{
      path: '/usersonline',
      name: 'usersonline',
      component: UsuariosOnline,
      beforeEnter:Auth
    },
    {
      path: '/aplicacoes',
      name: 'aplicacoes',
      component: Aplicacoes,
      beforeEnter:Auth
    },
    {
      path: '/clientes',
      name: 'clientes',
      component: Clientes,
      beforeEnter:Auth
    },
    {
      path: '/planos',
      name: 'planos',
      component: Planos,
      beforeEnter:Auth
    },
    {
      path: '/licencas',
      name: 'licencas',
      component: Licencas,
      beforeEnter:Auth
    },
    {
      path: '/pagamentos',
      name: 'pagamentos',
      component: Pagamentos,
      beforeEnter:Auth
    },
    {
      path: '/revendas',
      name: 'revendas',
      component: Revendas,
      redirect: '/revenda-normalizador',
      beforeEnter:Auth,
      children:[
        {
          path: '/revenda-normalizador',
          name: 'revendanormalizador',
          component: RevendaNormalizador,
          beforeEnter:Auth
        },
        {
          path: '/revenda-astandes',
          name: 'revendaastandes',
          component: RevendaAstandes,
          beforeEnter:Auth
        }
      ]
    },
    {
      path: '*',
      name: '404',
      component: Error404
    }*/
  ]
})
