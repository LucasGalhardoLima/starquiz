export default {
  async carregarPersonagens({ getters, commit }, page) 
  {
    let { data } = await getters.getApi.get(`people/?page=${page}`)
     if (data) commit('SET_PERSONAGENS', data)
    return data
    
  },

  async carregarPlaneta({ getters, commit }, planeta) 
  {
    let { data } = await getters.getApi.get(`planets/${planeta}`)
    //console.log(data)
    if (data) commit('SET_PLANETA', data)
    return data
    
  },

  async carregarSpecie({ getters, commit }, specie) 
  {
    let { data } = await getters.getApi.get(`species/${specie}`)
    //console.log(data)
    if (data) commit('SET_SPECIE', data)
    return data
    
  },

  async carregarFilmes({ getters, commit }, filmes) 
  {
    let arr = []
    for(let i in filmes)
    {
      let { data } = await getters.getApi.get(`films/${filmes[i]}`)

      arr.push(data)
    }
    if (arr.length > 0) commit('SET_MOVIES', arr)
    return arr
    
  },

  async carregarVeiculos({ getters, commit }, veiculos) 
  {
    let arr = []
    for(let i in veiculos)
    {
      let { data } = await getters.getApi.get(`vehicles/${veiculos[i]}`)

      arr.push(data)
    }
    if (arr.length > 0) commit('SET_VEICULOS', arr)
    return arr
    
  },
  

}