export default {
    getPersonagens: state => state.personagens,
    getApi: state => state.$api,
    getPlaneta: state => state.planeta,
    getSpecie: state => state.specie,
    getMovies: state => state.movies,
    getVeiculos: state => state.veiculos
}