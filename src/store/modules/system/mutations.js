export default {
    'SET_PERSONAGENS'(state, data) {
        console.log(data)
        state.personagens = data
    },
    'SET_PLANETA'(state, data) {
        state.planeta = data
    },
    'SET_SPECIE'(state, data) {
        state.specie = data
    },

    'SET_MOVIES'(state, data) {
        state.movies = data
    },

    'SET_VEICULOS'(state, data) {
        state.veiculos = data
    }
}