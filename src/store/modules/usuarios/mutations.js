export default {
    'SET_USUARIOSEMPRESA' (state, usuarios) {
        state.usuariosempresa = usuarios
    },
    'SET_USUARIOS' (state, usuarios) {
        state.usuarios = usuarios
    },

    'SET_TIMELINE' (state, timeline) {
        state.timeline = timeline
    },

    'RESET_TIMELINE' (state) {
        state.timeline = []
    },

    'SET_ONLINESEMPRESA' (state, usuarios) {
        console.log(usuarios, 'mutation')
        state.usariosonlineempresa = usuarios
    },
    'SET_ONLINESAPP' (state, usuarios) {
        state.usuariosonlineapp = usuarios
    },

    'SET_ONLINES' (state, usuarios) {
        state.usuariosonline = usuarios
    }
}