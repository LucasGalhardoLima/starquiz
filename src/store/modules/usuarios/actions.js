let source
export default {
    async usersOnline({commit, getters}, data)
    {
        ///let { data, status } = await getters.getApi.get(`loginServer/usersonline`)
        //commit('SET_ONLINES', data)
        //return data.status
        if(data)
        {
            source = new EventSource(process.env.VUE_APP_API_URL + 'loginServer/usersonline')
            source.onmessage = e => {
                let dados = JSON.parse( e.data )

                if(dados)commit('SET_ONLINES', dados)
                //source.close()
            }
        }
        else{
            source.close()
            console.log(data)
        }

        
        
    },

    async timelineUser({commit, getters}, {usuario_id, data_min, data_max, app_id})
    {
        let { data, status } = await getters.getApi.get(`loginServer/timeline/usuario/${usuario_id}/${data_min}/${data_max}/${app_id}`)
        commit('SET_TIMELINE', data)
        return data.status
    },

    async limpaTimeline({commit})
    {
      await commit('RESET_TIMELINE')
    },

    async loadUsersOnlineEmpresa({commit, getters}, empresa_id)
    {
        let { data, status } = await getters.getApi.get(`loginServer/usersonline/${empresa_id}`)
        commit('SET_ONLINESEMPRESA', data)
        return data.status
    },

    async loadUsersOnlineApp({commit, getters}, app_id)
    {
        let { data, status } = await getters.getApi.get(`loginServer/usersonline/app/${app_id}`)
        commit('SET_ONLINESAPP', data)
        return data.status
    },
    
    async loadUsuarios({commit, getters}, empresa_id)
    {
        let { data, status } = await getters.getApi.get(`loginServer/usuarios/${empresa_id}`)
        commit('SET_USUARIOSEMPRESA', data)
        return data.status
    },

    async listaTodosUsuarios({commit, getters})
    {
        let { data, status } = await getters.getApi.get(`loginServer/usuarios`)
        commit('SET_USUARIOS', data)
        return data.status
    },

    async saveUsuario({dispatch, commit, getters}, usuario)
    {
        let { data, status } = await getters.getApi.post(`loginServer/usuarios`, usuario)
        dispatch('loadUsuarios', usuario.empresa_id)
        return data.status
    },

    async updateUsuario({dispatch, commit, getters}, usuario)
    {
        let { data, status } = await getters.getApi.put(`loginServer/usuarios`, usuario)
        dispatch('loadUsuarios', usuario.empresa_id)
        return data.status
    },

    async removeUser({dispatch, commit, getters}, usuario )
    {
        let { data, status } = await getters.getApi.delete(`loginServer/usuarios/${usuario._id}`)
        dispatch('loadUsuarios', usuario.empresa_id)
        return data.status
    }
}