export default {
    getUsuariosEmpresa: state => state.usuariosempresa,
    getUsuarios: state => state.usuarios,
    getUsuariosOnlineEmpresa: state => state.usariosonlineempresa,
    getUsuariosOnlineApp: state => state.usuariosonlineapp,
    getUsuariosOnline: state => state.usuariosonline,
    getTimeline: state => state.timeline
}