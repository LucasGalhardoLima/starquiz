import system from './modules/system/index.js'
import usuarios from './modules/usuarios/index.js'


export default { 
	modules: {
		system, usuarios
	}
}
