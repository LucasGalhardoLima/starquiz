import store from '../store/index.js'

function requireAuth (to, from, next)
{
  console.log( to.name )
  store.modules.system.state.token    = sessionStorage.getItem("token")
  store.modules.system.state.usuario  = JSON.parse( sessionStorage.getItem("usuario") ) || false

  return !store.modules.system.state.token ? next("/") : next()
}
export default requireAuth