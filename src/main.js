import Vue from 'vue'
import Vuex from 'vuex'
import App from './App.vue'
import Vuetify from 'vuetify'
import router from './router'
import VuexStore from './store'
import axios from "axios"
import EventHub from 'vue-event-hub'
import tinymce from 'vue-tinymce-editor'
import VueQuillEditor from 'vue-quill-editor'
import VueChartkick from 'vue-chartkick'
import VueGoogleCharts from 'vue-google-charts'
import Chart from 'chart.js'
import '@fortawesome/fontawesome-free/css/all.css'
import VueNumeric from 'vue-numeric'
import VueTheMask from 'vue-the-mask'



// require styles
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'

Vue.use(Vuetify, {
  iconfont: 'fa'
 })
Vue.use(EventHub)
Vue.use(VueTheMask)
Vue.use(Vuex)
Vue.use(Vuetify)
Vue.use(VueNumeric)
Vue.use(VueGoogleCharts)
Vue.use(VueChartkick)
Vue.component('tinymce', tinymce)
Vue.use(VueQuillEditor, /* { default global options } */)
Vue.config.productionTip = true

import 'vuetify/dist/vuetify.min.css'
import 'material-design-icons/iconfont/material-icons.css'

const store = new Vuex.Store(VuexStore)
let {state} = VuexStore.modules.system

state.$api  = axios.create({
  baseURL: process.env.VUE_APP_API_URL
})

state.$api.interceptors.request.use( config => {
  config.headers.common['Authorization'] = state.token
  return config
}, error => {
  return Promise.reject(error)
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
